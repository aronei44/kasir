<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Master_barang extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    
    public function transaksi_pembelian_barangs(){
    	return $this->hasMany(Transaksi_pembelian_barang::class);
    }
}
