<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi_pembelian_barang extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    public function transaksi_pembelian(){
    	return $this->belongsTo(Transaksi_Pembelian::class);
    }
    public function master_barang(){
    	return $this->belongsTo(Master_barang::class);
    }
    public $timestamps = false;
}
