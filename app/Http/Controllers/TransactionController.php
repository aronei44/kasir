<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Master_barang;
use App\Models\Transaksi_pembelian;
use App\Models\Transaksi_pembelian_barang;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pdf()
    {
        $data['data'] = Transaksi_pembelian::with('transaksi_pembelian_barangs')->get();
        $pdf = \PDF::loadView('invoice', $data);
        return $pdf->download('invoice.pdf');   
    }


    public function index()
    {
        if(auth()->user()->profile->isAdmin){
            return view('transaksi',[
                'transaksi'=>Transaksi_pembelian::all()
            ]);
        }else{
            return view('transaksi',[
                'transaksi'=>Transaksi_pembelian::where('created_at','like',date('Y-m-d').'%')->get()
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create',['items'=>Master_barang::all()]);      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = Transaksi_pembelian::create([
            'total_harga'=>$request->total
        ]);
        foreach ($request->id_barang as $key => $value) {
            Transaksi_pembelian_barang::create([
                'transaksi_pembelian_id'=>$item->id,
                'master_barang_id'=>$value,
                'jumlah'=>$request->jumlah_barang[$key],
                'harga_satuan'=>$request->harga_satuan[$key]
            ]);
        }
        return redirect('/transaksi/'.$item->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return Transaksi_pembelian::with('transaksi_pembelian_barangs')->find($id);
        return view('show',
            ['transaksi'=>Transaksi_pembelian::with('transaksi_pembelian_barangs')
                                            ->find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
