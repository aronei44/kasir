<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;


use App\Models\User;
use App\Models\Profile;

class UserController extends Controller
{
    public function loginView(){
    	return view('login');
    }
    public function login(Request $request){
    	$validatedData = $request->validate([
            'email'=>'required',
            'password'=>'required'
        ]);
        if(Auth::attempt($validatedData)){
            $request->session()->regenerate();
            return redirect()->intended('/');
        }
 
    }
    function registerView() {
        return view('register');
    }
    function register(Request $request){
        if($request->isAdmin == env('APP_KEY')){
            $validatedData = $request->validate([
                'name'      => 'required|min:8|max:45',
                'email'     => 'required|email|unique:users',
                'password'  => 'required|min:8',
                // 'isAdmin'	=> ''
            ]);
        	// return $request;
            $validatedData['password']  = Hash::make($validatedData['password']);
            $user = User::create($validatedData);
            Profile::create([
            	'isAdmin'=>1,
            	'user_id'=>$user->id
            ]);
            return redirect('/login');
        }
        return back();
    }
     public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }
}
