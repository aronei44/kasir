<?php

namespace App\Http\Controllers;

use App\Models\Master_barang;
use Illuminate\Http\Request;


class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('barang', ['items'=>Master_barang::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'nama_barang'=>'required|string|min:3|max:45',
            'harga_satuan'=>'required|numeric'
        ]);
        Master_barang::create($data);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master_barang  $master_barang
     * @return \Illuminate\Http\Response
     */
    public function show(Master_barang $master_barang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master_barang  $master_barang
     * @return \Illuminate\Http\Response
     */
    public function edit(Master_barang $master_barang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master_barang  $master_barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $master_barang)
    {
        $item = Master_barang::find($master_barang);
        // dd($item);

        $item->update([
            'nama_barang'=>$request->nama_barang,
            'harga_satuan'=>$request->harga_satuan
        ]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master_barang  $master_barang
     * @return \Illuminate\Http\Response
     */
    public function destroy($master_barang)
    {
        Master_barang::find($master_barang)->delete();
        return back();
    }
}
