<!DOCTYPE html>
<html>
<head>
	<title>Laporan Transaksi</title>
</head>
<body>
	@foreach($data as $key => $value)
		<h1>Laporan Transaksi Ke {{$key+1}}</h1>
		<br>
		<h3>Waktu : {{$value->created_at}}</h3>
		<table border="1">
			<tr>
				<td>No</td>
				<td>Nama Barang</td>
				<td>Harga Barang</td>
				<td>Jumlah</td>
			</tr>
			@foreach($value->transaksi_pembelian_barangs as $num => $barang)
				<tr>
					<td>{{$num+1}}</td>
					<td>{{$barang->master_barang->nama_barang}}</td>
					<td>{{$barang->harga_satuan}}</td>
					<td>{{$barang->jumlah}}</td>
				</tr>
			@endforeach
			<h3>Total : {{$value->total_harga}}</h3>
		</table>
	@endforeach
</body>
</html>