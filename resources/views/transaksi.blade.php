@extends('main')

@section('header')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Transaksi</h1>
</div>
@endsection

@section('content')
	<!-- Button trigger modal -->
	<a href="/transaksi/create" class="btn btn-primary">
		<i class="fas fa-plus"></i> Transaksi Baru
	</a>

	<div class="bg-white container mt-5 mb-5">
		<table class="table table-hover" id="myTable">
			<thead>
				<tr>
					<td>No</td>
					<td>ID Transaksi</td>
					<td>Waktu Transaksi</td>
					<td>Total Harga</td>
					<td>Aksi</td>
				</tr>
			</thead>
			<tbody>
				@foreach($transaksi as $key => $value)
				<tr>
					<td>{{$key+1}}</td>
					<td>{{$value->id}}</td>
					<td>{{$value->created_at}}</td>
					<td style="text-align: right;">Rp {{$value->total_harga}}</td>
					<td><a href="/transaksi/{{$value->id}}" class="btn btn-primary"><i class="fas fa-eye"></i></a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection

@push('script')
<script>
	$(document).ready( function () {
	    $('#myTable').DataTable();
	} );
</script>
@endpush