@extends('main')

@section('header')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800 hide">Detail Transaksi</h1>
</div>
@endsection

@section('content')
	<button type="button" class="btn btn-success hide" onclick="window.print()"><i class="fas fa-print"></i> Print</button>
	<div class="bg-white pt-5 pb-5 pr-5 pl-5" style="width: 300px">
		<?php $waktu = explode(' ', $transaksi->created_at);?>
		<h5 class="text-center">JCC Shop</h5>
		<p>Tanggal : {{$waktu[0]}}</p>
		<p>Jam : {{$waktu[1]}}</p>
		<table class="table">
			<thead>
				<tr>
					<td>Nama</td>
					<td>Jumlah</td>
					<td>Harga (Rp)</td>
				</tr>
			</thead>
			<tbody>
				@foreach($transaksi->transaksi_pembelian_barangs as $key => $trans)
				<tr>
					<td>{{$trans->master_barang->nama_barang}}</td>
					<td id="jumlah{{$key}}">{{$trans->jumlah}}</td>
					<td id="harga{{$key}}" class="text-right">{{$trans->harga_satuan}}</td>
				</tr>
				@endforeach
				<tr>
					<td><h4>Total </h4></td>
					<td class="text-right" colspan="2">Rp {{$transaksi->total_harga}}</td>
				</tr>
			</tbody>
		</table>
		<p class="text-center">Terimakasih Sudah Berbelanja</p>
	</div>
@endsection
@push('script')
<script>
	let num = 0
	setTimeout(()=>{
		let sub = document.querySelectorAll('.sub')
		sub.forEach(()=>{
			document.getElementById('sub'+num).innerHTML = 'Rp ' + document.getElementById('jumlah'+num).innerHTML*document.getElementById('harga'+num).innerHTML
			num++
		})
	},1)
</script>
@endpush