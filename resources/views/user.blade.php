@extends('main')

@section('header')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">User Management System</h1>
</div>
@endsection

@section('content')
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addUser">
  <i class="fas fa-plus"></i> User
</button>

<!-- Modal -->
<div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="addUserlabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addUserlabel">Tambah User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/user" method="post">
        @csrf
        <div class="modal-body">
          <div class="form-group">
            <label for="name">Username</label>
            <input type="name" name="name" id="name" class="form-control @error('name') @enderror">
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" id="email" class="form-control @error('email') @enderror">
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" class="form-control @error('password') @enderror">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>

      </form>
    </div>
  </div>
</div>

<div class="container bg-white pt-3 pb-3">
  <table class="table table-hover" id="myTable">
    <thead>
      <tr>
        <td>No</td>
        <td>Email</td>
        <td>Username</td>
        <td>Status</td>
        <td>Aksi</td>
      </tr>
    </thead>
    <tbody>
      @foreach($users as $key => $user)
        <tr>
          <td>{{$key+1}}</td>
          <td>{{$user->email}}</td>
          <td>{{$user->name}}</td>
          <td>{{$user->profile->isAdmin ? 'Admin' : 'Kasir'}}</td>
          <td>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editUser{{$user->id}}">
              <i class="fas fa-edit"></i>
            </button>
            <form action="/user/{{$user->id}}" method="post">
              @csrf
              @method('delete')
              <button class="btn btn-danger" type="submit"><i class="fas fa-trash"></i></button>
            </form>
          </td>
        </tr>
        <div class="modal fade" id="editUser{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="editUserLabel{{$user->id}}" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="editUserLabel{{$user->id}}">Edit User ({{$user->email}})</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form action="/user/{{$user->profile->id}}" method="post">
                @csrf
                @method('put')
                <div class="modal-body">
                  <div class="form-group">
                    <label for="isAdmin">Status</label>
                    <select name="isAdmin" id="isAdmin" class="form-control">
                      <option value="1" {{$user->profile->isAdmin == 1 ? 'selected' : ''}}>Admin</option>
                      <option value="0" {{$user->profile->isAdmin == 0 ? 'selected' : ''}}>Kasir</option>
                    </select>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      @endforeach
    </tbody>
  </table>
</div>
@endsection
@push('script')
<script>
  $(document).ready( function () {
      $('#myTable').DataTable();
  } );
</script>
@endpush