       <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion hide" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
                <div class="sidebar-brand-text mx-3">App Kasir</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Umum
            </div>
            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/transaksi">
                    <i class="fas fa-fw fa-clipboard-list"></i>
                    <span>Transaksi</span>
                </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/barang">
                    <i class="fas fa-fw fa-calendar"></i>
                    <span>Barang</span>
                </a>
            </li>
            @if(auth()->user()->profile->isAdmin)
            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Admin
            </div>
            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="/user">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Pengguna</span>
                </a>
            </li>
            @endif

        </ul>