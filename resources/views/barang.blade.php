@extends('main')

@section('header')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Barang</h1>
</div>
@endsection

@section('content')
	<!-- Button trigger modal -->
	@if(auth()->user()->profile->isAdmin)
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newTrans">
	  <i class="fas fa-plus"></i> Barang Baru
	</button>
	@endif
	<div class="bg-white container mt-5 mb-5">
		<table class="table table-hover" id="myTable">
			<thead>
				<tr>
					<td>ID Barang</td>
					<td>Nama Barang</td>
					<td>Harga Satuan</td>
					@if(auth()->user()->profile->isAdmin)
					<td>Aksi</td>
					@endif
				</tr>
			</thead>
			<tbody>
				@foreach($items as $item)
				<tr>
					<td>{{$item->id}}</td>
					<td>{{$item->nama_barang}}</td>
					<td style="text-align: right;">Rp {{$item->harga_satuan}}</td>
					@if(auth()->user()->profile->isAdmin)
					<td>
						<!-- Button trigger modal -->
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#lihat{{$item->id}}">
						  <i class="fas fa-edit"></i>
						</button>

						<!-- Modal -->
						<div class="modal fade" id="lihat{{$item->id}}" tabindex="-1" aria-labelledby="lihat{{$item->id}}label" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="lihat{{$item->id}}label">Edit Barang</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <form action="/barang/{{$item->id}}" method="post">
						      	@csrf
						      	@method('put')

							      <div class="modal-body">
							        <div class="form-group">
							        	<label for="nama_barang">Nama Barang</label>
							        	<input type="text" name="nama_barang" id="nama_barang" class="form-control" value="{{$item->nama_barang}}">
							        </div>
							        <div class="form-group">
							        	<label for="harga_satuan">Harga Satuan</label>
							        	<input type="number" name="harga_satuan" id="harga_satuan" class="form-control" value="{{$item->harga_satuan}}">
							        </div>
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							        <button type="submit" class="btn btn-primary">Save changes</button>
							      </div>
						      </form>
						    </div>
						  </div>
						</div>
					</td>
					@endif
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="newTrans" tabindex="-1" aria-labelledby="newTransLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="newTransLabel">Tambah Barang</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <form action="/barang" method="post">
	      	@csrf

		      <div class="modal-body">
		        <div class="form-group">
		        	<label for="nama_barang">Nama Barang</label>
		        	<input type="text" name="nama_barang" id="nama_barang" class="form-control">
		        </div>
		        <div class="form-group">
		        	<label for="harga_satuan">Harga Satuan</label>
		        	<input type="number" name="harga_satuan" id="harga_satuan" class="form-control">
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="submit" class="btn btn-primary">Save changes</button>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>
@endsection
@push('script')
<script>
	$(document).ready( function () {
	    $('#myTable').DataTable();
	} );
</script>
@endpush