@extends('main')

@section('header')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Buat Transaksi Baru</h1>
</div>
@endsection

@section('content')
	<div class="bg-white container mt-5 mb-5 pt-3 pb-3">
		<div class="row mb-3 mt-3">
			<div class="col-md-2">
				<label for="select">Pilih Barang</label>
			</div>
			<div class="col-md-5">
				<select id="select" class="form-control">
					@foreach($items as $item)
					<option value="{{$item->id}}">{{$item->nama_barang}}</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-3">
				<button type="button" class="btn btn-primary" onclick="tambah_barang()">Tambah Barang</button>
			</div>
		</div>
		<p id="tes"></p>
		<form action="/transaksi" method="post">
			<table class="table">
				<thead>
					<tr>
						<td>No</td>
						<td>Id Barang</td>
						<td>Nama Barang</td>
						<td>Harga Barang</td>
						<td>Jumlah Barang</td>
						<td colspan="2">Sub Total</td>
					</tr>
				</thead>
					@csrf

					<tbody id="tbody">

					</tbody>

			</table>
			<hr>
			<div class="row justify-content-end">
				<div class="col-2">
					<h4>Total</h4>
				</div>
				<div class="col-1">
					Rp
				</div>
				<div class="col-2">
					<input type="number" name="total" id="total" readonly>
				</div>
				<div class="col-1"></div>
			</div>
			<div class="row justify-content-end mt-3 mb-3">
				<div class="col-3">
					
					<button type="submit" class="btn btn-primary">Proses</button>
				</div>
			</div>
		</form>
	</div>

@endsection
@push('script')
	<script type="text/javascript">
		const item = []
		let num = 0
		const tambah_barang = () =>{
			const idbarang = document.getElementById('select').value
			if(item.includes(parseInt(idbarang))){
				Swal.fire({
				  title: 'Warning!',
				  text: 'Item Sudah Ada',
				  icon: 'warning',
				  confirmButtonText: 'Oke'
				})
			}else{
				num++
				fetch('/item/'+idbarang)
				.then(response=> response.json())
				.then(data=>{
					document.getElementById('tbody').innerHTML += `<tr>
						<td>${num}</td>
						<td><input type="hidden" name="id_barang[]" value="${data.id}">${data.id}</td>
						<td>${data.nama_barang}</td>
						<td><input type="hidden" name="harga_satuan[]" id="harga${data.id}" value="${data.harga_satuan}">${data.harga_satuan}</td>
						<td>
							<a href="#" onclick="kurang(${data.id})"><i class="fas fa-minus"></i></a>
							<input type="number" name="jumlah_barang[]" style="width: 70px" id="jumlah${data.id}" value="1" min="0">
							<a href="#" onclick="tambah(${data.id})"><i class="fas fa-plus"></i></a>
						</td>
						<td>Rp</td>
						<td id="subtotal${data.id}" class="sub"></td>
					</tr>`
					item.push(data.id)
				})
			}
		}
		setInterval(()=>{
			let harga = 0
			if(item.length>=1){
				item.forEach(data=>{
					document.getElementById('subtotal'+data).innerHTML = document.getElementById('jumlah'+data).value*document.getElementById('harga'+data).value
				})
				let sub = document.querySelectorAll('.sub')
				sub.forEach(data=>harga+=parseInt(data.innerHTML))
			}
			document.getElementById('total').value = harga
		},100)
		const tambah = (id) =>{
			document.getElementById('jumlah'+id).value ++
		}
		const kurang = (id) =>{
			let val = document.getElementById('jumlah'+id).value
			if(val>0){
				document.getElementById('jumlah'+id).value--
			}
		}
	</script>
	<script>
		$(document).ready(function() {
		    $('#select').select2();
		});
	</script>
@endpush