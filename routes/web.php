<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UsersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// base path
Route::get('/', function () {
	$uang = 0;
	foreach (App\Models\Transaksi_pembelian::all() as $value) {
		$uang+=$value->total_harga;
	}
	$pendapatanHariIni = 0;
	foreach (App\Models\Transaksi_pembelian::where('created_at','like',date('Y-m-d'.'%'))->get() as $value) {
		$pendapatanHariIni += $value->total_harga;
	}
	$item = count(App\Models\Master_barang::all());
	$transaksi = count(App\Models\Transaksi_pembelian::all());
	$transaksiHariIni = count(App\Models\Transaksi_pembelian::where('created_at','like',date('Y-m-d'.'%'))->get());
    return view('index',compact('uang','item','transaksi','pendapatanHariIni','transaksiHariIni'));
})->middleware('auth')->name('home');


// just some api
Route::get('/item/{id}', function ($id) {
	return App\Models\Master_barang::find($id);
})->middleware('auth');


// route resource
Route::resource('/transaksi', TransactionController::class)->middleware('auth');
Route::resource('/barang', ItemController::class)->middleware('auth');
Route::resource('/user', UsersController::class)->middleware('auth');


Route::get('/pdf', [TransactionController::class, 'pdf'])->middleware('auth');


// Log path
Route::get('/login', [UserController::class, 'loginView'])->name('login')->middleware('guest');
Route::get('/register', [UserController::class, 'registerView'])->middleware('guest');

Route::post('/login', [UserController::class, 'login']);
Route::post('/register', [UserController::class, 'register']);

Route::get('/logout', [UserController::class, 'logout']);

