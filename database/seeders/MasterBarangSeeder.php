<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use App\Models\Master_barang;

class MasterBarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('master_barangs')->insert([
        	'nama_barang'=>'Sabun Batang',
        	'harga_satuan'=>3000
        ]);
        DB::table('master_barangs')->insert([
        	'nama_barang'=>'Mie Instan',
        	'harga_satuan'=>2000
        ]);
        DB::table('master_barangs')->insert([
        	'nama_barang'=>'Pensil',
        	'harga_satuan'=>1000
        ]);
        DB::table('master_barangs')->insert([
        	'nama_barang'=>'Kopi Sachet',
        	'harga_satuan'=>1500
        ]);
        DB::table('master_barangs')->insert([
        	'nama_barang'=>'Air Minum Galon',
        	'harga_satuan'=>20000
        ]);
    }
}
