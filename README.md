### Aplikasi Kasir

## Tentang Aplikasi Ini

Framework : laravel 8

Versi : 1.0.0

Contributor : [aronei44](https://gitlab.com/aronei44), [kamilersz](https://gitlab.com/kamilersz)

## Instalasi

- `git clone https://gitlab.com/aronei44/kasir.git`

- `cd kasir`

- `composer install or composer update`

- `php artisan key:generate`

- `php artisan migrate --seed`

- `php artisan serve`

## Penggunaan

- buka localhost:8000 atau 127.0.0.1:8000 atau kasir.test

- buka file env. copy app key. dimulai dari base64 sampai akhir untuk register admin

- selamat menggunakan
